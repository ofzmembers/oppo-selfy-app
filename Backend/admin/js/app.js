function getData(){
    $.ajax({
        url: 'http://dev.modernie.com/sandbox/Sahan/oppo-selfy-app/admin/php/',
        data: {
            action: 'getImages'
        },
        error: function() {
            console.log("error");
        },
        success: function(result) {;
            var data = jQuery.parseJSON(result);
            $('.admin-table').empty();
            if(data){
                console.log(data);
                for(var i =0;i < data.length; i++){
                    //data[i].url
                    var hiddenUndoClass;
                    var hiddenAcceptClass;
                    if(data[i].status == 0){
                        hiddenUndoClass = "hidden";
                        hiddenAcceptClass = "visible";
                    }
                    if(data[i].status == 1){
                        hiddenAcceptClass = "hidden";
                        hiddenUndoClass = "visible";
                    }

                    var appendText =
                     '<tr>' +
                        '<td class="text-td">'+ data[i].id +'</td>' +
                        '<td class="text-td"><img src="http://dev.modernie.com/sandbox//Sahan/oppo-selfy-app/app-backend/uploads/'+data[i].photo_name+'" class="img-responsive os-image-thumb"> </td>'+
                        '<td class="text-td">'+ data[i].photo_name +'</td>' +
                        '<td class="text-td hidden-xs">'+ data[i].created_time +'</td>' +
                        '<td class="text-td hidden-xs">'+ data[i].status +'</td>' +
                        '<td>' +
                            '<div class="cust-group pull-right" role="group">' +

                                '<button class="btn btn-warning '+ hiddenAcceptClass +'"  onclick="acceptRequerst('+ data[i].id +');">' +
                                    '<i class="fa fa-check"></i>' +
                                '</button>' +
                                '<button class="btn btn-danger '+ hiddenAcceptClass +'"  onclick="rejectRequerst('+ data[i].id +');">' +
                                    '<i class="fa fa-times" ></i>' +
                                '</button>' +
                                '<button class="btn btn-success '+ hiddenUndoClass +'"  onclick="undoRequerst('+ data[i].id +');">' +
                                    '<i class="fa fa-undo" ></i>' +
                                '</button>' +
                            '</div>' +
                        '</td>' +
                    '</tr>';
                    $('.admin-table').append(appendText);

                }
            }
        },
        type: 'POST'
    });
}

function sendAction(a,d){
    $.ajax({
        url: 'http://dev.modernie.com/sandbox/Sahan/oppo-selfy-app/admin/php/',
        data: {
            action: a,
            id:d
        },
        error: function() {
            console.log("error");
        },
        success: function(result) {;
            var data = jQuery.parseJSON(result);
            $('.admin-table').empty();
            if(data){
                console.log(data);
                for(var i =0;i < data.length; i++){
                    //data[i].url
                    var hiddenUndoClass;
                    var hiddenAcceptClass;
                    if(data[i].status == 0){
                        hiddenUndoClass = "hidden";
                        hiddenAcceptClass = "visible";
                    }
                    if(data[i].status == 1){
                        hiddenAcceptClass = "hidden";
                        hiddenUndoClass = "visible";
                    }

                    var appendText =
                        '<tr>' +
                        '<td class="text-td">'+ data[i].id +'</td>' +
                        '<td class="text-td"><img src="http://dev.modernie.com/sandbox//Sahan/oppo-selfy-app/app-backend/uploads/'+data[i].photo_name+'" class="img-responsive os-image-thumb"> </td>'+
                        '<td class="text-td">'+ data[i].photo_name +'</td>' +
                        '<td class="text-td hidden-xs">'+ data[i].created_time +'</td>' +
                        '<td class="text-td hidden-xs">'+ data[i].status +'</td>' +
                        '<td>' +
                            '<div class="cust-group pull-right" role="group">' +
                                '<button class="btn btn-warning '+ hiddenAcceptClass +'"  onclick="acceptRequerst('+ data[i].id +');">' +
                                '<i class="fa fa-check"></i>' +
                                '</button>' +
                                '<button class="btn btn-danger '+ hiddenAcceptClass +'"  onclick="rejectRequerst('+ data[i].id +');">' +
                                '<i class="fa fa-times" ></i>' +
                                '</button>' +
                                '<button class="btn btn-success '+ hiddenUndoClass +'" onclick="undoRequerst('+ data[i].id +');">' +
                                '<i class="fa fa-undo" ></i>' +
                                '</button>' +
                            '</div>' +
                        '</td>' +
                        '</tr>';
                    $('.admin-table').append(appendText);


                }
            }
            //notify(a,d);
        },
        type: 'POST'
    });
}


function acceptRequerst(id){
    sendAction('acceptRequerst',id);
}

function rejectRequerst(id){
    sendAction('rejectRequerst',id);
}

function undoRequerst(id){
    sendAction('undoRequerst',id);
}


$(document).ready(function(){
    getData();

});

function reloadData(){
    getData();
}

function notify(a,d){
    new PNotify({
        title: a,
        text: 'Update success on ' + d,
        type: 'success',
        icon: true,
        buttons: {closer: true}
    });
}

